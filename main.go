package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"
	"image"
	"image/color"
	"log"
	"math/rand"
	"time"
)

type (
	snakeT struct {
		paused                 bool
		running                bool
		directionChangeBlocked bool
		direction              rune
		score                  uint
		blocks                 []image.Point
		food                   image.Point
		nextTitle              string
	}
)

const (
	blockSize     = 8
	width         = 32 // in blocks
	height        = 32
	fieldWidth    = width * blockSize
	fieldHeight   = height * blockSize
	initialLength = 3   // square blocks of blockSize*blockSize px
	movementSpeed = 200 // ms to move 1 block
)

var (
	snake            snakeT
	blockImage       *ebiten.Image
	foodImage        *ebiten.Image
	animationStarted bool
)

// Main input/render function called per each game tick
func update(screen *ebiten.Image) (err error) {
	// Check if ebiten tells us to skip drawing
	if ebiten.IsDrawingSkipped() {
		return
	}

	// Only start the animation process once the rendering happens for the first time.
	if !animationStarted {
		go animate()
		animationStarted = true
	}

	// Repaint the screen white
	if err := screen.Fill(color.White); err != nil {
		return err
	}

	// Render the food
	// TODO: draw a circle instead of a square
	opts := &ebiten.DrawImageOptions{}
	opts.GeoM.Translate(float64(snake.food.X*blockSize), float64(snake.food.Y*blockSize))
	_ = screen.DrawImage(foodImage, opts)

	// Render the snake
	for _, block := range snake.blocks {
		opts := &ebiten.DrawImageOptions{}
		opts.GeoM.Translate(float64(block.X*blockSize), float64(block.Y*blockSize))
		_ = screen.DrawImage(blockImage, opts)
	}

	// Do the title update, if one was scheduled
	if snake.nextTitle != "" {
		ebiten.SetWindowTitle(snake.nextTitle)
		snake.nextTitle = ""
	}

	// Rendering is processed. Parse input events.
	// Game over state.
	if !snake.running {
		// Check for game restart having been requested (an arrow key or space pressed).
		if inpututil.IsKeyJustReleased(ebiten.KeyUp) ||
			inpututil.IsKeyJustReleased(ebiten.KeyRight) ||
			inpututil.IsKeyJustReleased(ebiten.KeyDown) ||
			inpututil.IsKeyJustReleased(ebiten.KeyLeft) ||
			inpututil.IsKeyJustReleased(ebiten.KeySpace) {
			// Wait for some time so that key is sure to be reset, and, eg. restarting by pressing "up"
			// doesn't result in game being over instantly
			time.Sleep(300 * time.Millisecond)
			initSnake()
		}

		return
	}

	// Direction change
	if !snake.directionChangeBlocked {
		if ebiten.IsKeyPressed(ebiten.KeyUp) && snake.direction != 'd' {
			snake.direction = 'u'
			// block another direction change until an animate() cycle passes.
			snake.directionChangeBlocked = true
		} else if ebiten.IsKeyPressed(ebiten.KeyRight) && snake.direction != 'l' {
			snake.direction = 'r'
			snake.directionChangeBlocked = true
		} else if ebiten.IsKeyPressed(ebiten.KeyDown) && snake.direction != 'u' {
			snake.direction = 'd'
			snake.directionChangeBlocked = true
		} else if ebiten.IsKeyPressed(ebiten.KeyLeft) && snake.direction != 'r' {
			snake.direction = 'l'
			snake.directionChangeBlocked = true
		}
	}

	// Pause/unpause.
	if inpututil.IsKeyJustReleased(ebiten.KeySpace) {
		snake.paused = !snake.paused

		if snake.paused {
			snake.nextTitle = "Paused"
		} else {
			snake.nextTitle = getScoreTitleStr()
		}
	}

	return
}

func isBlockInSnake(block *image.Point) bool {
	for _, b := range snake.blocks {
		if *block == b {
			return true
		}
	}
	return false
}

func getScoreTitleStr() string {
	return fmt.Sprintf("Score: %d", snake.score)
}

// Game process logic function started in goroutine
func animate() {
	for {
		time.Sleep(movementSpeed * time.Millisecond)
		if snake.paused {
			continue
		}

		snake.directionChangeBlocked = false

		x, y := snake.blocks[0].X, snake.blocks[0].Y
		switch snake.direction {
		case 'u':
			y--
		case 'r':
			x++
		case 'd':
			y++
		case 'l':
			x--
		}
		newHead := image.Point{X: x, Y: y}

		// Game over on collision with a wall or self.
		var gameOver bool
		if x < 0 || y < 0 || x >= width || y >= height {
			gameOver = true
		}
		if isBlockInSnake(&newHead) {
			gameOver = true
		}
		if gameOver {
			snake.running = false
			snake.nextTitle = fmt.Sprintf("Game over! Score: %d", snake.score)
			return
		}

		var newSnakeTail []image.Point
		if newHead == snake.food {
			// Food was eaten
			snake.score++
			snake.nextTitle = getScoreTitleStr()
			placeFood()

			newSnakeTail = snake.blocks
		} else {
			newSnakeTail = snake.blocks[:len(snake.blocks)-1]
		}

		snake.blocks = append([]image.Point{newHead}, newSnakeTail...)
	}
}

func placeFood() {
	for {
		food := image.Point{
			X: rand.Intn(width),
			Y: rand.Intn(height),
		}
		// Don't generate food twice at the same place
		if food == snake.food {
			continue
		}
		// Don't generate food inside the snake
		if isBlockInSnake(&food) {
			continue
		}
		snake.food = food
		break
	}
}

func initSnake() {
	var snakeBlockArr [width * height]image.Point
	snake = snakeT{
		false,
		true,
		false,
		'r',
		0,
		snakeBlockArr[:0],
		image.Point{X: 0, Y: 0},
		"",
	}
	snake.nextTitle = getScoreTitleStr()
	// snake.blocks starts from the snake's head; food is implied to be added at its' end.
	for i := initialLength - 1; i >= 0; i-- {
		snake.blocks = append(snake.blocks, image.Point{X: i, Y: 0})
	}
	placeFood()
	animationStarted = false
}

func main() {
	initSnake()

	// Init the snake block image
	blockImage, _ = ebiten.NewImage(blockSize, blockSize, ebiten.FilterDefault)
	if err := blockImage.Fill(color.Black); err != nil {
		log.Fatal(err)
	}

	// Init the snake food image
	foodImage, _ = ebiten.NewImage(blockSize, blockSize, ebiten.FilterDefault)
	if err := foodImage.Fill(color.Black); err != nil {
		log.Fatal(err)
	}

	// Main loop
	if err := ebiten.Run(update, fieldWidth, fieldHeight, 1, string(snake.score)); err != nil {
		log.Fatal(err)
	}
}
